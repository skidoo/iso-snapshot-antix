<?xml version="1.0" ?><!DOCTYPE TS><TS language="sk" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="57"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="850"/>
        <location filename="ui_mainwindow.h" line="549"/>
        <source>Snapshot</source>
        <translation>Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="550"/>
        <source>Snapshot location:</source>
        <translation>Zložka obrazu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot je aplikácia na tvorbu spustiteľných (ISO) obrazov vášho funkčného systému, ktoré môžete použiť pre zálohovanie alebo distribúciu. Počas tvorby zálohy môžete paralelne používať nemej náročné aplikácie.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="552"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Použitý priestor na diskoch / (root) a /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="796"/>
        <location filename="ui_mainwindow.h" line="556"/>
        <source>Snapshot name:</source>
        <translation>Názov obrazu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="557"/>
        <source>Select a different snapshot directory</source>
        <translation>Zvoľte inú zložku pre obraz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="561"/>
        <source>lz4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <location filename="ui_mainwindow.h" line="562"/>
        <source>lzo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <location filename="ui_mainwindow.h" line="563"/>
        <source>gzip</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <location filename="ui_mainwindow.h" line="564"/>
        <source>xz</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="566"/>
        <location filename="ui_mainwindow.h" line="568"/>
        <source>TextLabel</source>
        <translation>Textové označenie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="569"/>
        <source>Downloads</source>
        <translation>Stiahnuté</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <location filename="ui_mainwindow.h" line="570"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="352"/>
        <location filename="ui_mainwindow.h" line="571"/>
        <source>All of the above</source>
        <translation>Všetko uvedené vyššie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="359"/>
        <location filename="ui_mainwindow.h" line="572"/>
        <source>Pictures</source>
        <translation>Obrázky</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <location filename="ui_mainwindow.h" line="573"/>
        <source>Music</source>
        <translation>Hudba</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="574"/>
        <source>Desktop</source>
        <translation>Pracovná plocha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="ui_mainwindow.h" line="575"/>
        <source>Videos</source>
        <translation>Videá</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="ui_mainwindow.h" line="577"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Táto voľba zresetuje heslá pre &amp;quot;demo&amp;quot; a &amp;quot;root&amp;quot; na antiX Linux predvolené a neprenesie žiadne uživateľom vytvorené kontá.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <location filename="ui_mainwindow.h" line="579"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Resetovať kontá (distribúcia iným)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="580"/>
        <source>Type of snapshot:</source>
        <translation>Typ obrazu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <location filename="ui_mainwindow.h" line="581"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Zachovať kontá (pre vlastné zazálohovanie)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>ISO compression scheme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Edit Exclusion File</source>
        <translation>Upraviť zoznam vynechaných</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <location filename="ui_mainwindow.h" line="586"/>
        <source>Display help </source>
        <translation>Zobraziť nápovedu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <location filename="ui_mainwindow.h" line="588"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <source>About this application</source>
        <translation>O tejto aplikácii</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="635"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>About...</source>
        <translation>O Programe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="658"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Quit application</source>
        <translation>Zatvoriť aplikáciu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="668"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="ui_mainwindow.h" line="601"/>
        <source>Next</source>
        <translation>Nasledujúce</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="603"/>
        <source>Back</source>
        <translation>Späť</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Used space on / (root): </source>
        <translation>Použitý priestor na disku / (root):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Used space on /home: </source>
        <translation>Použitý priestor na disku /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Voľné miesto na disku %1, na ktorom je obraz umiestnený: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Voľné miesto musí byť dostatočné pre umiestnenie komprimovaných dát z diskov / a /home

Ak to bude nutné, môžete uvoľnit miesto
odstránením predošlých obrazov alebo kópií systému:
%1 obrazy zaberajú %2 diskového priestoru.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Installing </source>
        <translation>Inštalácia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <location filename="mainwindow.cpp" line="592"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="802"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <source>Could not install </source>
        <translation>Nepodarilo sa nainštalovať</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Building new initrd...</source>
        <translation>Tvorba nového initrd...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopíruje sa nový ISO systém súborov....</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="589"/>
        <source>Squashing filesystem...</source>
        <translation>Tvorba systému súborov...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Nepodarilo sa vytvoriť súbor linuxfs, prosím skontrolujte, či máte v cieľom umiestnení dostatok voľného miesta </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Vytváranie CD/DVD obrazu...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Nepodarilo sa vytvoriť ISO súbor, prosím skontrolujte, či máte v cieľom umiestnení dostatok voľného miesta.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Making hybrid iso</source>
        <translation>Tvorba hybridného ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="628"/>
        <source>MX Snapshot completed sucessfully!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="629"/>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Snapshot took %1 to finish.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="631"/>
        <source>Thanks for using MX Snapshot, run MX Live USB Maker next!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Making md5sum</source>
        <translation>Tvorba kontrolného súboru md5sum</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="668"/>
        <source>Cleaning...</source>
        <translation>Čistenie...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="988"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="770"/>
        <source>Please wait.</source>
        <translation>Čakajte prosím...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="772"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Čakajte prosím, vypočítava sa použité miesto na disku...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Použijú sa nasledujúce nastavenia:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <source>- Snapshot directory:</source>
        <translation>- Zložka pre umiestnenie obrazu:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>- Kernel to be used:</source>
        <translation>- Linuxové jadro:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="807"/>
        <source>Final chance</source>
        <translation>Posledná šanca</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot má všetky potrebné informácie k vytvoreniu súboru ISO z vášho funkčného systému.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="809"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Tvorba obrazu zaberie nejaký čas, v závislosti na veľkosti nainštalovaného systému a výkone vášho PC. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="810"/>
        <source>OK to start?</source>
        <translation>Chcete začať?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="819"/>
        <source>Output</source>
        <translation>Výstup</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Edit Boot Menu</source>
        <translation>Upraviť štartovnú ponuku</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Program sa pozastaví aby Vám umožnil upraviť súbory v pracovnej zložke. Zvoľte Ano pre editáciu štartovnej ponuky alebo Nie ak si prajete preskočiť tento krok a pokračovať v tvorbe obrazu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>Success</source>
        <translation>Úspech</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>All finished!</source>
        <translation>Hotovo!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="841"/>
        <source>Close</source>
        <translation>Zavrieť</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="942"/>
        <source>About %1</source>
        <translation>Okolo %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <source>Version: </source>
        <translation>Verzia:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="944"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Program pre tvorbu Live-CD z funkčného operačného systému antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="947"/>
        <source>%1 License</source>
        <translation>%1 Licencia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>%1 Help</source>
        <translation>%1 Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="970"/>
        <source>Select Snapshot Directory</source>
        <translation>Zvoľte zložku pre snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Confirmation</source>
        <translation>Potvrdenie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Skutočne si prajete ukončiť program?</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.h" line="567"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation>Vybraním položiek nižšie môžete zo zálohy vynechať určité zložky alebo kliknutím na tlačidlo priamo upraviť ich zoznam definovaný v súbore /usr/lib/iso-snapshot/snapshot-exclude.list.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>História zmien</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvoriť</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You must run this program as root.</source>
        <translation>Tento program musíte spustiť ako root užívateľ.</translation>
    </message>
</context>
</TS>