<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="57"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="850"/>
        <location filename="ui_mainwindow.h" line="549"/>
        <source>Snapshot</source>
        <translation>Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="550"/>
        <source>Snapshot location:</source>
        <translation>Snapshot locatie:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is een software-gereedschap waarmee een bootable  (ISO)-bestand van je werkend systeem kan worden gemaakt dat kan worden gebruikt als  opslag of distributie. Je kunt blijven doorwerken met niet al te zware applicaties terwijl iso-snapshot bezig is. &lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="552"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Gebruikte ruimte op / (root) en /home partities:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="796"/>
        <location filename="ui_mainwindow.h" line="556"/>
        <source>Snapshot name:</source>
        <translation>Snapshot naam:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="557"/>
        <source>Select a different snapshot directory</source>
        <translation>Selecteer een andere snapshot folder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="561"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <location filename="ui_mainwindow.h" line="562"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <location filename="ui_mainwindow.h" line="563"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <location filename="ui_mainwindow.h" line="564"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="566"/>
        <location filename="ui_mainwindow.h" line="568"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>U kunt ook zekere directories uitsluiten door de algemene keuzes hieronder aan te vinken, of door op de knop te klikken om rechtstreeks /usr/local/share/excludes/iso-snapshot-exclude.list te bewerken.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="569"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <location filename="ui_mainwindow.h" line="570"/>
        <source>Documents</source>
        <translation>Documenten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="352"/>
        <location filename="ui_mainwindow.h" line="571"/>
        <source>All of the above</source>
        <translation>Al het bovenstaande</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="359"/>
        <location filename="ui_mainwindow.h" line="572"/>
        <source>Pictures</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <location filename="ui_mainwindow.h" line="573"/>
        <source>Music</source>
        <translation>Muziek</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="574"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="ui_mainwindow.h" line="575"/>
        <source>Videos</source>
        <translation>Video&apos;s</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="ui_mainwindow.h" line="577"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deze optie zal &quot;demo&quot; en &quot;root&quot; wachtwoorden resetten naar de standaardwaarden van antiX Linux en zal geen persoonlijke accounts die gecreëerd zijn kopieëren.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <location filename="ui_mainwindow.h" line="579"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Accounts opnieuw instellen (voor distributie naar anderen)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="580"/>
        <source>Type of snapshot:</source>
        <translation>Type snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <location filename="ui_mainwindow.h" line="581"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Account bewaren (voor persoonlijke backup)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>ISO compression scheme:</source>
        <translation>ISO compressieschema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Edit Exclusion File</source>
        <translation>Bewerk Exclusion Bestand</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <location filename="ui_mainwindow.h" line="586"/>
        <source>Display help </source>
        <translation>Toon help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <location filename="ui_mainwindow.h" line="588"/>
        <source>Help</source>
        <translation>Hulp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <source>About this application</source>
        <translation>Over deze toepassing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="635"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>About...</source>
        <translation>Over...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="658"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Quit application</source>
        <translation>Verlaat de applicatie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Cancel</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="668"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="ui_mainwindow.h" line="601"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="603"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Used space on / (root): </source>
        <translation>Gebruikte ruimte op / (root):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Used space on /home: </source>
        <translation>Gebruikte ruimte op /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Vrije ruimte op %1, waar de snapshot folder geplaatst is:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>De vrije ruimte zou afdoende moeten zijn om de gecomprimeerde data van / en /home te bevatten

Indien nodig kunt u meer beschikbare ruimte creëren
door oudere snapshots en opgeslagen kopieën te verwijderen:
%1 snapshots nemen %2 van de diskruimte in beslag.

</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Installing </source>
        <translation>Installeren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <location filename="mainwindow.cpp" line="592"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="802"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <source>Could not install </source>
        <translation>Kon niet installeren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Building new initrd...</source>
        <translation>Nieuwe initrd bouwen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>New-iso bestandssysteem kopiëren...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="589"/>
        <source>Squashing filesystem...</source>
        <translation>Comprimeren van het bestandsysteem</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Kon geen linuxfs bestand creëren, controleer aub of u genoeg ruimte op de doelpartitie heeft.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Creating CD/DVD image file...</source>
        <translation>CD/DVD image bestand maken...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Kon geen ISO bestand creëren, controleer aub of u genoeg ruimte op de doelpartitie heeft.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Making hybrid iso</source>
        <translation>Hybride iso maken</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="628"/>
        <source>MX Snapshot completed sucessfully!</source>
        <translation>MX Snapshot succesvol voltooid!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="629"/>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Snapshot nam %1 tijd in beslag.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="631"/>
        <source>Thanks for using MX Snapshot, run MX Live USB Maker next!</source>
        <translation>Dank u voor het gebruiken van MX Snapshot, voer MX Live USB Maker hierna uit!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Making md5sum</source>
        <translation>Md5sum maken</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="668"/>
        <source>Cleaning...</source>
        <translation>Opruimen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="988"/>
        <source>Done</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="770"/>
        <source>Please wait.</source>
        <translation>Even wachten aub.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="772"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Even wachten aub. Gebruikte schijfruimte aan het berekenen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot zal de volgende instellingen gebruiken:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <source>- Snapshot directory:</source>
        <translation>- Snapshot folder:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>- Kernel to be used:</source>
        <translation>- Te gebruiken kernel:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Huidige kernel ondersteunt het geselecteerde compressie algoritme niet, pas a.u.b. het configuratiebestand aan en kies een ander algoritme.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="807"/>
        <source>Final chance</source>
        <translation>Laatste kans</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot heeft nu alle informatie die het nodig heeft om een ISO van uw lopende systeem te creëren. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="809"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Dit zal enige tijd duren, afhankelijk van de afmeting van het geïnstalleerde systeem en de capaciteit van uw computer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="810"/>
        <source>OK to start?</source>
        <translation>OK om te beginnen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="819"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Edit Boot Menu</source>
        <translation>Boot Menu aanpassen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Het programma zal nu pauzeren om u in staat te stellen bestanden aan te passen in de werk folder. Selecteer Ja om het opstartmenu aan te passen of selecteer Nee om deze stap over te slaan en door te gaan met het creëren van het snapshot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>Success</source>
        <translation>Gelukt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>All finished!</source>
        <translation>Helemaal klaar!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="841"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="942"/>
        <source>About %1</source>
        <translation>Over %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <source>Version: </source>
        <translation>Versie:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="944"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programma om een live-CD te creëren van het lopende systeem voor antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="947"/>
        <source>%1 License</source>
        <translation>%1 Licentie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>%1 Help</source>
        <translation>%1 Help</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="970"/>
        <source>Select Snapshot Directory</source>
        <translation>Selecteer Snapshot Folder</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Confirmation</source>
        <translation>Bevestiging</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Weet u zeker dat u wilt stoppen met deze applicatie?</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.h" line="567"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation>U kunt ook zekere directories uitsluiten door de algemene keuzes hieronder aan te vinken, of door op de knop te klikken om rechtstreeks /usr/lib/iso-snapshot/snapshot-exclude.list te bewerken.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licentie</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Huidige kernel ondersteunt geen Squashfs, kan niet doorgaan.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You must run this program as root.</source>
        <translation>U dient deze toepassing als &apos;root&apos; uit te voeren.</translation>
    </message>
</context>
</TS>
