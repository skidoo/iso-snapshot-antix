<?xml version="1.0" ?><!DOCTYPE TS><TS language="da" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="57"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="850"/>
        <location filename="ui_mainwindow.h" line="549"/>
        <source>Snapshot</source>
        <translation>Øjebliksbillede</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="550"/>
        <source>Snapshot location:</source>
        <translation>Placering af øjebliksbillede:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Øjebliksbillede er et redskab som opretter et opstartsbart aftryk (ISO) af det system du arbejder med som du kan bruge til at gemme eller distribuere. Mens det kører kan du fortsætte arbejde med programmer som ikke kræver meget.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="552"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Anvendt plads på /- (rod) og /home-partitioner:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="796"/>
        <location filename="ui_mainwindow.h" line="556"/>
        <source>Snapshot name:</source>
        <translation>Navn på øjebliksbillede:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="557"/>
        <source>Select a different snapshot directory</source>
        <translation>Vælg en anden mappe til øjebliksbillede</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="561"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <location filename="ui_mainwindow.h" line="562"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <location filename="ui_mainwindow.h" line="563"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <location filename="ui_mainwindow.h" line="564"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="566"/>
        <location filename="ui_mainwindow.h" line="568"/>
        <source>TextLabel</source>
        <translation>Tekstetiket</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Du kan også ekskludere bestemte mapper ved at tilvælge de fælles valg nedenfor eller ved at klikke på knappen for direkte redigering af /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="569"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <location filename="ui_mainwindow.h" line="570"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="352"/>
        <location filename="ui_mainwindow.h" line="571"/>
        <source>All of the above</source>
        <translation>Allesammen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="359"/>
        <location filename="ui_mainwindow.h" line="572"/>
        <source>Pictures</source>
        <translation>Billeder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <location filename="ui_mainwindow.h" line="573"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="574"/>
        <source>Desktop</source>
        <translation>Skrivebord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="ui_mainwindow.h" line="575"/>
        <source>Videos</source>
        <translation>Videoer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="ui_mainwindow.h" line="577"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Valgmuligheden nulstiller &amp;quot;demo&amp;quot;- og &amp;quot;root&amp;quot;-adgangskoderne til standarderne for antiX Linux og vil kopiere personlige konti som er blevet oprettet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <location filename="ui_mainwindow.h" line="579"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Nulstiller konti (til distribution til andre)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="580"/>
        <source>Type of snapshot:</source>
        <translation>Øjebliksbilledetype:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <location filename="ui_mainwindow.h" line="581"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Bevar konti (til personlig sikkerhedskopiering)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>ISO compression scheme:</source>
        <translation>ISO-komprimeringsskema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Edit Exclusion File</source>
        <translation>Rediger eksklusionsfil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <location filename="ui_mainwindow.h" line="586"/>
        <source>Display help </source>
        <translation>Vis hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <location filename="ui_mainwindow.h" line="588"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <source>About this application</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="635"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Alt+B</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="658"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Quit application</source>
        <translation>Afslut programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="668"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="ui_mainwindow.h" line="601"/>
        <source>Next</source>
        <translation>Næste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="603"/>
        <source>Back</source>
        <translation>Tilbage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Used space on / (root): </source>
        <translation>Anvendt plads på / (rod): </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Used space on /home: </source>
        <translation>Anvendt plads på /home: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Ledig plads på %1, til placering af mappe til øjebliksbillede: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Den ledige plads bør være nok til de komprimerede data fra / og /home

      Hvis det er nødvendigt, kan du oprette mere tilgængelig plads
      ved at fjerne tidligere øjebliksbilleder og gemte kopier:
      %1 øjebliksbilleder optager %2 displads.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Installing </source>
        <translation>Installerer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <location filename="mainwindow.cpp" line="592"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="802"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <source>Could not install </source>
        <translation>Kunne ikke installere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Building new initrd...</source>
        <translation>Bygger ny initrd...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopierer new-iso-filsystemet...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="589"/>
        <source>Squashing filesystem...</source>
        <translation>Squashing filsystem...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Kunne ikke oprette linuxfs-fil, tjek venligst om du har nok plads på destinationspartitionen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Opretter CD-/DVD-aftryksfil...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Kunne ikke oprette ISO-fil, tjek venligst om du har nok plads på destinationspartitionen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Making hybrid iso</source>
        <translation>Skaber hybrid-iso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="628"/>
        <source>MX Snapshot completed sucessfully!</source>
        <translation>MX Øjebliksbillede blev fuldført!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="629"/>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Øjebliksbillede tog %1 at fuldføre.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="631"/>
        <source>Thanks for using MX Snapshot, run MX Live USB Maker next!</source>
        <translation>Tak fordi du bruger MX Snapshot, kør nu MX Live USB-skaber!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Making md5sum</source>
        <translation>Skaber md5sum</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="668"/>
        <source>Cleaning...</source>
        <translation>Rydder op...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="988"/>
        <source>Done</source>
        <translation>Færdig</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="770"/>
        <source>Please wait.</source>
        <translation>Vent venligst.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="772"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Vent venligst. Udregner anvendt diskplads...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Settings</source>
        <translation>Indstillinger</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Øjebliksbillede bruger følgende indstillinger:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <source>- Snapshot directory:</source>
        <translation>- Mappe til øjebliksbillede:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>- Kernel to be used:</source>
        <translation>- Kerne som skal anvendes:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Den nuværende kerne understøtter ikke den valgte kompressionsalgoritme. Rediger venligst konfigurationsfilen og vælg en anden algoritme.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="807"/>
        <source>Final chance</source>
        <translation>Sidste chance</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Øjebliksbilledet har nu alle de informationer som er nødvendige for at oprette en ISO fra dit kørende system.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="809"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Det vil tage noget tid at færdiggøre, afhængig af størrelsen på det installeret system og kapaciteten på din computer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="810"/>
        <source>OK to start?</source>
        <translation>OK at starte?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="819"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Edit Boot Menu</source>
        <translation>Rediger opstatsmenu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Programmet sættes nu på pause så du har mulighed for at redigere filer i arbejdsmappen. Vælg Ja for at redigere opstartsmenuen eller vælg Nej for at omgå trinnet og fortsætte med at oprette øjebliksbilledet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>Success</source>
        <translation>Det lykkedes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>All finished!</source>
        <translation>Alt er færdigt!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="841"/>
        <source>Close</source>
        <translation>Luk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="942"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="944"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Program til at oprette en live-CD fra det kørende system til antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Ophavsret (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="947"/>
        <source>%1 License</source>
        <translation>%1-licens</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>%1 Help</source>
        <translation>%1-hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="970"/>
        <source>Select Snapshot Directory</source>
        <translation>Vælg mappe til øjebliksbillede</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Confirmation</source>
        <translation>Bekræftelse</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Er du sikker på, at du vil afslutte programmet?</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.h" line="567"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation>Du kan også ekskludere bestemte mapper ved at tilvælge de fælles valg nedenfor eller ved at klikke på knappen for direkte redigering af /usr/lib/iso-snapshot/snapshot-exclude.list.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Ændringslog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Den nuværende kerne understøtter ikke Squashfs, kan ikke fortsætte.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You must run this program as root.</source>
        <translation>Du skal køre programmet som root.</translation>
    </message>
</context>
</TS>