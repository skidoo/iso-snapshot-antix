<?xml version="1.0" ?><!DOCTYPE TS><TS language="sl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="57"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="850"/>
        <location filename="ui_mainwindow.h" line="549"/>
        <source>Snapshot</source>
        <translation>Posnetek</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="550"/>
        <source>Snapshot location:</source>
        <translation>Lokacija posnetka:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Posnetek je orodje za ustvarjanje zagonskega posnetka (ISO) vašega delujočega sistema, ki jo lahko uporabljate za shrambo ali distribuscijo.  Med njegovim delovanjem lahko uporabljate nezahtevne programe.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="552"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Uporabljen prostor na / (korenskem) in /home razdelku:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="796"/>
        <location filename="ui_mainwindow.h" line="556"/>
        <source>Snapshot name:</source>
        <translation>Ime posnetka:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="557"/>
        <source>Select a different snapshot directory</source>
        <translation>Določite drugo mapo posnetka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="561"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <location filename="ui_mainwindow.h" line="562"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <location filename="ui_mainwindow.h" line="563"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <location filename="ui_mainwindow.h" line="564"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="566"/>
        <location filename="ui_mainwindow.h" line="568"/>
        <source>TextLabel</source>
        <translation>TekstovnaOznaka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Lahko tudi izločite določene direktorije, tako da spodaj označite najbolj pogost izbor ali kliknete  na gumb, s katerim neposredno uredite izločitveni seznam /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="569"/>
        <source>Downloads</source>
        <translation>Prenosi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <location filename="ui_mainwindow.h" line="570"/>
        <source>Documents</source>
        <translation>Dokumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="352"/>
        <location filename="ui_mainwindow.h" line="571"/>
        <source>All of the above</source>
        <translation>Vse zgoraj našteto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="359"/>
        <location filename="ui_mainwindow.h" line="572"/>
        <source>Pictures</source>
        <translation>Slike</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <location filename="ui_mainwindow.h" line="573"/>
        <source>Music</source>
        <translation>Glasba</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="574"/>
        <source>Desktop</source>
        <translation>Namizje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="ui_mainwindow.h" line="575"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="ui_mainwindow.h" line="577"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta opcija bo ponastavila &amp;quot;demo&amp;quot; in &amp;quot;rkorensko&amp;quot; geslo na privzeto in ne bo kopirala ustvarjenih osebnih računov.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <location filename="ui_mainwindow.h" line="579"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Ponastavitev računov (za distribucijo drugim)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="580"/>
        <source>Type of snapshot:</source>
        <translation>Vste posnetkov:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <location filename="ui_mainwindow.h" line="581"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Ohranijanje računov (za osebne varnostne kopije)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>ISO compression scheme:</source>
        <translation>Sheman za kompresiranje ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Edit Exclusion File</source>
        <translation>Uredi izločitvene datoteke</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <location filename="ui_mainwindow.h" line="586"/>
        <source>Display help </source>
        <translation>Prikaži pomoč</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <location filename="ui_mainwindow.h" line="588"/>
        <source>Help</source>
        <translation>Pomoč</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <source>About this application</source>
        <translation>O tem programu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="635"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>About...</source>
        <translation>O programu...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="658"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Quit application</source>
        <translation>Zapri aplikacijo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="668"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="ui_mainwindow.h" line="601"/>
        <source>Next</source>
        <translation>Naslednji</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="603"/>
        <source>Back</source>
        <translation>Nazaj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Used space on / (root): </source>
        <translation>Porabljen prostor na / (koren):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Used space on /home: </source>
        <translation>Porabljen prostor na /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Sprostite prostor na %1, na katerem se bo nahajala mapa posnetka:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Prazen prostor bi moral zadostovati, da lahko vsebuje stisnjene potake za / in /home

</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Installing </source>
        <translation>Nameščanje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <location filename="mainwindow.cpp" line="592"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="802"/>
        <source>Error</source>
        <translation>Napaka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <source>Could not install </source>
        <translation>Namesitev ni uspela</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Building new initrd...</source>
        <translation>Ustvarjam nov initrd...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopiranje novega iso datotečnega sistema</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="589"/>
        <source>Squashing filesystem...</source>
        <translation>Stiskam datotečni sistem...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Neuspelo ustvarjanje linuxfs datoteke. Preverite ali je na ciljnem razdelku dovolj prostora.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Ustvarjam CD/DVD podobo...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Neuspelo ustvarjanje ISO datoteke. Preverite ali je na ciljnem razdelku dovolj prostora.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Making hybrid iso</source>
        <translation>Ustvarjam hibridni iso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="628"/>
        <source>MX Snapshot completed sucessfully!</source>
        <translation>MX posnetek je uspešno zaključil delo!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="629"/>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Posnetek je potreboval %1 za dokončanje.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="631"/>
        <source>Thanks for using MX Snapshot, run MX Live USB Maker next!</source>
        <translation>Hvala, ker uporabljate MX posnetek. Zdaj zaženite MX ustvarjalec živih usb nosilcev</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Making md5sum</source>
        <translation>Ustvarjam md5sum</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="668"/>
        <source>Cleaning...</source>
        <translation>Čistim...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="988"/>
        <source>Done</source>
        <translation>Zaključeno</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="770"/>
        <source>Please wait.</source>
        <translation>Prosimo, počakajte.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="772"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Prosimo, počakajte. Izračunavam porabo prostora na disku...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Posnetek bo uporabljal naslednje nastavitve:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <source>- Snapshot directory:</source>
        <translation>- Mapa posnetka:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>- Kernel to be used:</source>
        <translation>- Uporabljeno jedro:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Trenutno jedro ne podpira izbranega algoritma za stiskanje podatkov. Prosimo, uredite konfiguracijsko datoteko in izberite drug algoritem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="807"/>
        <source>Final chance</source>
        <translation>Poslednja priložnost</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Posnetek ima sedaj vse potrebne informacije, da ustvari ISO iz vašega sistema.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="809"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Za dokončanje je potrebnega nekaj časa, odvisno od velikosti nameščenega sistema in zmogljivosti vašega računalnika.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="810"/>
        <source>OK to start?</source>
        <translation>Lahko začnem?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="819"/>
        <source>Output</source>
        <translation>Izhod</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Edit Boot Menu</source>
        <translation>Uredi zagonski meni</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Program se bo sedaj zaustavil in omogočil urejanje datotek v delovnem imeniku. Izberite Yes/ da za urejanje zagonskega menija ali No/ ne, če želite zaobiti ta korak in nadaljevati ustvarjanje posnetka.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>Success</source>
        <translation>Operacija je uspela</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>All finished!</source>
        <translation>Vse je končano!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="841"/>
        <source>Close</source>
        <translation>Zapri</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="942"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <source>Version: </source>
        <translation>Različica:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="944"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Program za ustvarjanje živih CD iz zagnanega sistema.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="947"/>
        <source>%1 License</source>
        <translation>%1 licenca</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>%1 Help</source>
        <translation>%1 pomoč</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="970"/>
        <source>Select Snapshot Directory</source>
        <translation>Določite mapo posnetka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Confirmation</source>
        <translation>Potrditev</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Ali ste prepričani, da želite zapustiti aplikacijo?</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.h" line="567"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation>Lahko tudi izločite določene direktorije, tako da spodaj označite najbolj pogost izbor ali kliknete  na gumb, s katerim neposredno uredite izločitveni seznam  /usr/lib/iso-snapshot/snapshot-exclude.list.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Dnevnik sprememb</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zapri</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation>Napaka</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Trenutno jedro ne podpira Squashfs. Ne morem nadaljevati.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You must run this program as root.</source>
        <translation>Ta program morate zagnati korensko</translation>
    </message>
</context>
</TS>