<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="57"/>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="850"/>
        <location filename="ui_mainwindow.h" line="549"/>
        <source>Snapshot</source>
        <translation>Zrzut dysku</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="550"/>
        <source>Snapshot location:</source>
        <translation>Lokalizacja zrzutu danych:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;MX Zrzut systemu tworzy obraz uruchomieniowy ISO Twojego systemu (backup, dysk ratunkowy, redystrybucja). Podczas jego pracy staraj się nie korzystać z innych aplikacji mocno obciążających Twój system.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="552"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Wykorzystane miejsce na partycji głównej (root) / i partycji /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="796"/>
        <location filename="ui_mainwindow.h" line="556"/>
        <source>Snapshot name:</source>
        <translation>Nazwa pliku obrazu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="557"/>
        <source>Select a different snapshot directory</source>
        <translation>Wybierz inny katalog zrzutu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="561"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="220"/>
        <location filename="ui_mainwindow.h" line="562"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <location filename="ui_mainwindow.h" line="563"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <location filename="ui_mainwindow.h" line="564"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="566"/>
        <location filename="ui_mainwindow.h" line="568"/>
        <source>TextLabel</source>
        <translation>Etykieta Tekstowa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="281"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Możesz także wykluczyć niektóre katalogi, zaznaczając poniższe popularne opcje lub klikając przycisk, aby bezpośrednio edytować /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="569"/>
        <source>Downloads</source>
        <translation>Pobrane</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <location filename="ui_mainwindow.h" line="570"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="352"/>
        <location filename="ui_mainwindow.h" line="571"/>
        <source>All of the above</source>
        <translation>Wszystkie powyższe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="359"/>
        <location filename="ui_mainwindow.h" line="572"/>
        <source>Pictures</source>
        <translation>Obrazy</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <location filename="ui_mainwindow.h" line="573"/>
        <source>Music</source>
        <translation>Muzyka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="574"/>
        <source>Desktop</source>
        <translation>Pulpit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="ui_mainwindow.h" line="575"/>
        <source>Videos</source>
        <translation>Wideo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="ui_mainwindow.h" line="577"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta opcja zresetuje hasła użytkowników &quot;demo&quot; i &quot;root&quot; do domyślnych haseł antiX Linux i nie wykona kopii zapasowych jakichkolwiek danych prywatnych utworzonych użytkowników.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <location filename="ui_mainwindow.h" line="579"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Resetowanie kont (przydatne gdy chcesz podzielić się obrazem systemu z innymi)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="580"/>
        <source>Type of snapshot:</source>
        <translation>Typ zrzutu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <location filename="ui_mainwindow.h" line="581"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Zachowanie prywatnych danych (przydatne gdy wykonujesz kopię zapasową)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>ISO compression scheme:</source>
        <translation>Schemat kompresji ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Edit Exclusion File</source>
        <translation>Edytuj plik wykluczeń</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="571"/>
        <location filename="ui_mainwindow.h" line="586"/>
        <source>Display help </source>
        <translation>Wyświetl pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="574"/>
        <location filename="ui_mainwindow.h" line="588"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="581"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <source>About this application</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="635"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="642"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="658"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Quit application</source>
        <translation>Zakończ aplikację</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="668"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="ui_mainwindow.h" line="601"/>
        <source>Next</source>
        <translation>Dalej</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="603"/>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Used space on / (root): </source>
        <translation>Wykorzystane miejsce na partycji głównej (root) / : </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Used space on /home: </source>
        <translation>Wykorzystane miejsce na partycji /home: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Wolne miejsce na %1, zawierającej katalog zrzutu: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Ilość wolnej przestrzeni dyskowej powinna być wystarczająca do przechowywania
skompresowanych danych z / oraz /home. Jeśli to konieczne, uwolnij więcej miejsca
poprzez usunięcie poprzednich zrzutów i zapisanych kopii:
1% zrzuty zajmują %2 przestrzeni dyskowej.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Installing </source>
        <translation>Instalowanie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <location filename="mainwindow.cpp" line="592"/>
        <location filename="mainwindow.cpp" line="608"/>
        <location filename="mainwindow.cpp" line="802"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="291"/>
        <source>Could not install </source>
        <translation>Nie można zainstalować</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Building new initrd...</source>
        <translation>Budowanie nowego initrd ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopiowanie systemu plików nowego iso...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="589"/>
        <source>Squashing filesystem...</source>
        <translation>Kompresowanie systemu plików...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="592"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Nie można utworzyć pliku linuxfs, sprawdź, czy masz wystarczającą ilość miejsca na partycji docelowej.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Tworzenie pliku obrazu CD/DVD...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Nie można utworzyć pliku ISO, sprawdź, czy masz wystarczającą ilość miejsca na partycji docelowej.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Making hybrid iso</source>
        <translation>Tworzenie obrazu hybrydowego ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="628"/>
        <source>MX Snapshot completed sucessfully!</source>
        <translation>MX Zrzut systemu zakończony powodzeniem!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="629"/>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Wykonanie zrzutu systemu zajęło %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="631"/>
        <source>Thanks for using MX Snapshot, run MX Live USB Maker next!</source>
        <translation>Dziękujemy za korzystanie z MX Zrzut systemu, uruchom następnie MX Live USB Maker!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Making md5sum</source>
        <translation>Tworzenie sumy kontrolnej md5</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="668"/>
        <source>Cleaning...</source>
        <translation>Czyszczenie...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="988"/>
        <source>Done</source>
        <translation>Gotowe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="770"/>
        <source>Please wait.</source>
        <translation>Proszę czekać.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="772"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Proszę czekaj. Obliczanie wykorzystanej przestrzeni dyskowej...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="793"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Zrzut wykorzysta następujące ustawienia:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <source>- Snapshot directory:</source>
        <translation>- Katalog zrzutu:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>- Kernel to be used:</source>
        <translation>- Jądro systemu:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="803"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Bieżące jądro (kernel) nie obsługuje wybranego algorytmu kompresji, proszę edytować plik konfiguracyjny i wybrać inny algorytm.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="807"/>
        <source>Final chance</source>
        <translation>Ostatnia szansa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="808"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Zrzut zawiera teraz wszystkie informacje potrzebne do utworzenia obrazu ISO z Twojego systemu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="809"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Zakończenie pracy może zająć trochę czasu, w zależności od wielkości zainstalowanego systemu oraz możliwości sprzętowych Twojego komputera.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="810"/>
        <source>OK to start?</source>
        <translation>Zaczynamy?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="819"/>
        <source>Output</source>
        <translation>Dane wyjściowe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Edit Boot Menu</source>
        <translation>Edytuj menu rozruchu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Program zatrzyma pracę, by pozwolić Ci na edycję plików w katalogu roboczym. Wybierz Tak by edytować menu rozruchu lub Nie by pominąć ten krok i kontynuować tworzenie zrzutu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>Success</source>
        <translation>Sukces </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="840"/>
        <source>All finished!</source>
        <translation>Proces zakończony!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="841"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="942"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="943"/>
        <source>Version: </source>
        <translation>Wersja:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="944"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Program do tworzenia obrazów Live-CD z działającego systemu dla dystrybucji antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Prawa autorskie © MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="947"/>
        <source>%1 License</source>
        <translation>%1 Licencja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>%1 Help</source>
        <translation>%1 Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="970"/>
        <source>Select Snapshot Directory</source>
        <translation>Wybierz katalog zrzutu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Confirmation</source>
        <translation>Potwierdzenie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="989"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Czy na pewno chcesz zamknąć aplikację?</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.h" line="567"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/lib/iso-snapshot/snapshot-exclude.list.</source>
        <translation>Można również wykluczyć określone katalogi, zaznaczając je poniżej lub klikając przycisk, aby bezpośrednio edytować /usr/lib/iso-snapshot/snapshot-exclude.list.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Dziennik zmian</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Bieżące jądro (kernel) nie obsługuje Squashfs, nie można kontynuować.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You must run this program as root.</source>
        <translation>Musisz uruchomić ten program jako administrator. </translation>
    </message>
</context>
</TS>