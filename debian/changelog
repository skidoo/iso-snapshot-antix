iso-snapshot-antix (0.5) unstable; urgency=medium

  * merge recent changes from mx-snapshot
      work.cpp:   check xdg-user-dirs-update.real (localized "Desktop" folder namestring)
                  ( do not copy  /etc/skel if xdg-user-dirs-update.real exists )
      debian/rules:  add --no-automatic-dbgsym flag
      mainwindow.cpp: replace misleading (onSuccess) "Cancel" button with a "Close" button
      mainwindow.cpp: fix translation problem
  * about.cpp:    remove "Changelog" button ( on antiX, these are non-existent file(s) )
                  replace "Cancel" button text, with "Close"
  * translations:      nl.ts , pt.ts , tr.ts
      "mx-snapshot"---} "iso-snapshot"
  * iso-snapshot.conf: edited for clarity/readibility
  * help/mx-snapshot.html:
        ^---> renamed to "iso-snapshot.html"
      Selectively replaced occurrences of "MX" with "antiX".
      Replaced "broken" htmlentities (quotation chars and apostrophe char).
      Outcommented the TIPSnTRICKS section of the page. It mentions a broken//non-working (on antiX 19) "tip":
        $ sudo -E minstall
        ^----> sudo: sorry, you are not allowed to preserve the environment
      Expended the in-page content to include details which (would have been) present in the (mx)manpage.
      NOTED: The width of the embedded screenshots exceeded the opener width of the antiX-viewer window.
         ^--->  replaced screenshot images with cleaner/narrower PNGs, and narrowed the <body> witdh
  * iso-snapshot-exclude.list: add the following exclusion patterns
      etc/live/persist-config.conf
      etc/NetworkManager/system-connections/*
      etc/wicd/dhclient.conf.template.default
      etc/X11/xorg-bus-id
      root/.cache/qt_compose_cache_.*
      home/*/.cache/qt_compose_cache_.*
  * iso-snapshot.conf:
      correct misinformative detail ~~ snapshot excludes list is passed to MKSQUASHFS (not rsync)
  * iso-snapshot-exclude.list:
      Replaced incorrect mention of rsync.
      Revised/expanded the inline header comment (tips) lines.
      Added a few additional matchpatterns.

 -- skidoo <email@redact.ed>  Tue, 30 Mar 2021 12:17:17 +0200

iso-snapshot-antix (0.4.03) unstable; urgency=medium

  * more upstream buffixes, alterations

 -- anticapitalista <antix@operamail.com>  Thu, 11 Feb 2021 17:24:53 +0200

iso-snapshot-antix (0.4.02) unstable; urgency=medium

  * upstream bugfixes relates to snapshot directory

 -- anticapitalista <antix@operamail.com>  Tue, 02 Feb 2021 19:38:12 +0200

iso-snapshot-antix (0.4.01) unstable; urgency=medium

  * added firewire modules to copy-initrd-modules

 -- anticapitalista <antix@operamail.com>  Mon, 25 Jan 2021 18:45:44 +0200

iso-snapshot-antix (0.4.00) unstable; urgency=medium

  * cli option
  * man upstream changes

 -- anticapitalista <antix@operamail.com>  Mon, 18 Jan 2021 22:28:43 +0200

iso-snapshot-antix (0.3.25) unstable; urgency=medium

  * add checksum checkbox option
  * take ownership for snapshot folder and resulting files
  * some code fixes

 -- anticapitalista <antix@operamail.com>  Sun, 27 Dec 2020 18:05:29 +0200

iso-snapshot-antix (0.3.24) unstable; urgency=medium

  * various upstream chages

 -- anticapitalista <antix@operamail.com>  Sat, 19 Dec 2020 16:49:35 +0200

iso-snapshot-antix (0.3.23) unstable; urgency=medium

  * added gl_ES, zh_TW

 -- anticapitalista <antix@operamail.com>  Sat, 28 Nov 2020 17:14:52 +0200

iso-snapshot-antix (0.3.22) unstable; urgency=medium

  * exclude ~/.cache/thumbnails
  * add exclude Network configurations checkbox in GUI
  * improve editor selection

 -- anticapitalista <antix@operamail.com>  Tue, 24 Nov 2020 15:00:41 +0200

iso-snapshot-antix (0.3.21) unstable; urgency=medium

  * Removed MX references
  * restart as root (Adrian)

 -- anticapitalista <antix@operamail.com>  Tue, 03 Nov 2020 15:19:52 +0200

iso-snapshot-antix (0.3.20) unstable; urgency=medium

  * added postinstall script to symlink etc/iso-snapshot-exclude.list /usr/local/share/excludes/iso-snapshot-exclude.list

 -- anticapitalista <antix@operamail.com>  Wed, 28 Oct 2020 20:11:18 +0200

iso-snapshot-antix (0.3.19) unstable; urgency=medium

  * improved tr

 -- anticapitalista <antix@operamail.com>  Wed, 14 Oct 2020 14:29:49 +0300

iso-snapshot-antix (0.3.18) unstable; urgency=medium

  * improved pt_BR, added nb

 -- anticapitalista <antix@operamail.com>  Thu, 08 Oct 2020 16:12:39 +0300

iso-snapshot-antix (0.3.17) unstable; urgency=medium

  * added crc32 module for fsfs support

 -- anticapitalista <antix@operamail.com>  Sat, 03 Oct 2020 23:07:05 +0300

iso-snapshot-antix (0.3.16) unstable; urgency=medium

  * fix folder quoting

 -- anticapitalista <antix@operamail.com>  Sat, 06 Jun 2020 11:41:49 +0300

iso-snapshot-antix (0.3.15) unstable; urgency=medium

  * updated to upstream
  * edits to scripts files to incluse raid

 -- anticapitalista <antix@operamail.com>  Thu, 30 Apr 2020 14:47:44 +0300

iso-snapshot-antix (0.3.14) unstable; urgency=medium

  * replace gksu with su-to-root -X -c

 -- anticapitalista <antix@operamail.com>  Mon, 13 Jan 2020 17:12:05 +0200

iso-snapshot-antix (0.3.13) unstable; urgency=medium

  * use latest copy-to-initrd script

 -- anticapitalista <antix@operamail.com>  Mon, 09 Dec 2019 13:57:52 +0200

iso-snapshot-antix (0.3.12) unstable; urgency=medium

  * check using trailing slash

 -- anticapitalista <antix@operamail.com>  Wed, 30 Oct 2019 19:00:14 +0200

iso-snapshot-antix (0.3.11) unstable; urgency=medium

  * fix folder exclusion in /home
  * remove make-efi.image script

 -- anticapitalista <antix@operamail.com>  Thu, 24 Oct 2019 15:49:48 +0300

iso-snapshot-antix (0.3.10) unstable; urgency=medium

  * excludes.list now in /usr/local/share/excludes folder

 -- anticapitalista <antix@operamail.com>  Tue, 15 Oct 2019 15:48:00 +0300

iso-snapshot-antix (0.3.9) unstable; urgency=medium

  * buster release

 -- anticapitalista <antix@operamail.com>  Wed, 09 Oct 2019 23:22:35 +0300

iso-snapshot-antix (0.3.8) unstable; urgency=medium

  * added etc/X11/xorg-bus-id to excludes list

 -- anticapitalista <antix@operamail.com>  Fri, 04 Oct 2019 13:28:46 +0300

iso-snapshot-antix (0.3.7) unstable; urgency=medium

  * various upstream improvements
  * lz now default comprssion

 -- anticapitalista <antix@operamail.com>  Sun, 01 Sep 2019 11:42:06 +0100

iso-snapshot-antix (0.3.6) unstable; urgency=medium

  * buster release

 -- anticapitalista <antix@operamail.com>  Fri, 05 Jul 2019 21:04:52 +0300

iso-snapshot-antix (0.3.5) unstable; urgency=medium

  * fix correct path (etc) of iso-snapshot-exclude.list in iso-snapshot.conf

 -- anticapitalista <antix@operamail.com>  Sun, 02 Jun 2019 23:06:39 +0300

iso-snapshot-antix (0.3.4) unstable; urgency=medium

  * upstream improvements
  * add verion file
  * put excludes list file in etc
  * added virtio-gpu to copy-initrd-modules

 -- anticapitalista <antix@operamail.com>  Tue, 28 May 2019 14:53:50 +0300

iso-snapshot-antix (0.3.3) unstable; urgency=medium

  * use monospace font

 -- anticapitalista <antix@operamail.com>  Wed, 08 May 2019 15:13:58 +0300

iso-snapshot-antix (0.3.2) unstable; urgency=medium

  * synced with upstream
  * put scripts in /usr/share/iso-snapshot-antix and not /usr/share/packageinstaller

 -- anticapitalista <antix@operamail.com>  Tue, 07 May 2019 18:39:32 +0300

iso-snapshot-antix (0.3.1) unstable; urgency=medium

  * added hyperv modules to copy-initrd-modules script

 -- anticapitalista <antix@operamail.com>  Thu, 21 Mar 2019 17:33:37 +0200

iso-snapshot-antix (0.3.0) unstable; urgency=medium

  * antiX-18 build
  * changed default icon
  * exclude crypttab and encryption keyfiles in iso-snapshot-exclude.list (used with fde)
  * added xts to copy-initrd-modules

 -- anticapitalista <antix@operamail.com>  Mon, 10 Dec 2018 00:51:03 +0200

iso-snapshot-antix (0.2.10) unstable; urgency=medium

  * updated translations

 -- anticapitalista <antix@operamail.com>  Sun, 23 Sep 2018 22:27:24 +0300

iso-snapshot-antix (0.2.9) unstable; urgency=medium

  * fix getVersion

 -- anticapitalista <antix@operamail.com>  Sat, 23 Jun 2018 15:21:06 +0300

iso-snapshot-antix (0.2.8) unstable; urgency=medium

  * exclude localised user directories

 -- anticapitalista <antix@operamail.com>  Fri, 25 May 2018 12:54:27 +0300

iso-snapshot-antix (0.2.7) unstable; urgency=medium

  * Full Portuguese

 -- anticapitalista <antix@operamail.com>  Thu, 15 Mar 2018 19:33:01 +0200

iso-snapshot-antix (0.2.6) unstable; urgency=medium

  * updated translations

 -- anticapitalista <antix@operamail.com>  Fri, 02 Mar 2018 20:52:18 +0200

iso-snapshot-antix (0.2.5) unstable; urgency=medium

  * New translations added to .desktop file

 -- anticapitalista <antix@operamail.com>  Wed, 07 Feb 2018 21:32:18 +0200

iso-snapshot-antix (0.2.4) unstable; urgency=medium

  * added copy-initrd-programs

 -- anticapitalista <antix@operamail.com>  Mon, 15 Jan 2018 14:44:17 +0200

iso-snapshot-antix (0.2.3) unstable; urgency=medium

  * added var/cache/brightness-settings-cache/* to excludes.list

 -- anticapitalista <antix@operamail.com>  Fri, 12 Jan 2018 14:02:05 +0200

iso-snapshot-antix (0.2.2) unstable; urgency=medium

  * exclude localhost only for generalized snapshot

 -- anticapitalista <antix@operamail.com>  Thu, 02 Nov 2017 00:20:44 +0200

iso-snapshot-antix (0.2.1) unstable; urgency=medium

  * Various cosmetic changes made upstream in mx-snapshot
  * Lithuanian and Slovak added

 -- anticapitalista <antix@operamail.com>  Wed, 25 Oct 2017 14:37:47 +0300

iso-snapshot-antix (0.2.0) unstable; urgency=medium

  * bump version
  * add persist-config.conf to excludes list

 -- anticapitalista <antix@operamail.com>  Fri, 13 Oct 2017 12:43:35 +0300

iso-snapshot-antix (0.1.12) unstable; urgency=medium

  * Use capital letters in desktop file

 -- anticapitalista <antix@operamail.com>  Sat, 07 Oct 2017 21:04:17 +0300

iso-snapshot-antix (0.1.11) unstable; urgency=medium

  * New copy-initrd-modules script

 -- anticapitalista <antix@operamail.com>  Sat, 15 Jul 2017 12:03:42 +0100

iso-snapshot-antix (0.1.10) unstable; urgency=medium

  * Added exclusion for network connections

 -- anticapitalista <antix@operamail.com>  Sat, 24 Jun 2017 12:16:23 +0300

iso-snapshot-antix (0.1.9) unstable; urgency=medium

  * New copy-initrd-modules for encrytied live usb

 -- anticapitalista <antix@operamail.com>  Sat, 22 Apr 2017 02:45:29 +0300

iso-snapshot-antix (0.1.8) unstable; urgency=medium

  * Remove virtualbox-guest depends

 -- anticapitalista <antix@operamail.com>  Mon, 10 Apr 2017 20:48:39 +0300

iso-snapshot-antix (0.1.7) unstable; urgency=low

  * Added Czech and Portuguese translations

 -- anticapitalista <antix@operamail.com>  Sun, 02 Apr 2017 19:11:49 +0300

iso-snapshot-antix (0.1.6) unstable; urgency=medium

  * Many changes, clean ups to sync with upstream

 -- anticapitalista <antix@operamail.com>  Tue, 21 Feb 2017 10:57:49 +0200

iso-snapshot-antix (0.1.5) unstable; urgency=medium

  * apt-get -y install used

 -- anticapitalista <antix@operamail.com>  Sun, 05 Feb 2017 15:39:06 +0200

iso-snapshot-antix (0.1.4) unstable; urgency=medium

  * Updated copy-initrd-modules file

 -- anticapitalista <antix@operamail.com>  Sun, 08 Jan 2017 23:21:01 +0200

iso-snapshot-antix (0.1.3) unstable; urgency=medium

  * newer copy-to-init

 -- anticapitalista <antiX@operamail.com>  Wed, 06 Apr 2016 11:29:36 -0400

iso-snapshot-antix (0.1.2) unstable; urgency=medium

  * bigfixes

 -- anticapitalista <antiX@operamail.com>  Mon, 21 Mar 2016 07:05:09 -0400

iso-snapshot-antix (0.1.1) unstable; urgency=low

  * Added translations.

 -- anticapitalista <antiX@operamail.com>  Thu, 31 Dec 2015 19:52:00 +0200


iso-snapshot-antix (0.1) unstable; urgency=low

  * Initial Release.

 -- anticapitalista <antiX@operamail.com>  Mon, 28 Dec 2015 14:58:00 +0200
