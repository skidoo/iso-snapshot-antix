/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *selectionPage;
    QGridLayout *gridLayout_2;
    QLabel *labelLocation;
    QLabel *labelIntro;
    QLabel *labelCurrentSpace;
    QLabel *labelDiskSpaceHelp;
    QFrame *line_4;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *labelUsedSpace;
    QLabel *labelFreeSpace;
    QLabel *labelIsoName;
    QPushButton *buttonSelectSnapshot;
    QLabel *labelSnapshotDir;
    QSpacerItem *verticalSpacer;
    QLineEdit *lineEditName;
    QWidget *settingsPage;
    QGridLayout *gridLayout_4;
    QLabel *label_1;
    QFrame *line;
    QSpacerItem *verticalSpacer_2;
    QLabel *labelExclude;
    QFrame *line_5;
    QFrame *line_2;
    QLabel *label_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QCheckBox *excludeDownloads;
    QCheckBox *excludeDocuments;
    QCheckBox *excludePictures;
    QCheckBox *excludeMusic;
    QCheckBox *excludeDesktop;
    QCheckBox *excludeVideos;
    QCheckBox *excludeNetworks;
    QCheckBox *excludeAll;
    QSpacerItem *verticalSpacer_3;
    QFrame *frame_3;
    QGridLayout *gridLayout_3;
    QRadioButton *radioRespin;
    QLabel *label;
    QRadioButton *radioPersonal;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonEditExclude;
    QFrame *frame_4;
    QGridLayout *gridLayout_5;
    QComboBox *cbCompression;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_3;
    QLabel *labelCompression;
    QCheckBox *checksums;
    QWidget *outputPage;
    QVBoxLayout *verticalLayout_2;
    QTextEdit *outputBox;
    QLabel *outputLabel;
    QProgressBar *progressBar;
    QGridLayout *buttonBar;
    QPushButton *buttonHelp;
    QLabel *labelantiX;
    QSpacerItem *horizontalSpacer2;
    QPushButton *buttonAbout;
    QPushButton *buttonCancel;
    QPushButton *buttonNext;
    QSpacerItem *horizontalSpacer1;
    QPushButton *buttonBack;

    void setupUi(QDialog *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(798, 507);
        verticalLayout = new QVBoxLayout(MainWindow);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        stackedWidget = new QStackedWidget(MainWindow);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setFrameShadow(QFrame::Plain);
        selectionPage = new QWidget();
        selectionPage->setObjectName(QStringLiteral("selectionPage"));
        gridLayout_2 = new QGridLayout(selectionPage);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        labelLocation = new QLabel(selectionPage);
        labelLocation->setObjectName(QStringLiteral("labelLocation"));

        gridLayout_2->addWidget(labelLocation, 10, 0, 1, 1);

        labelIntro = new QLabel(selectionPage);
        labelIntro->setObjectName(QStringLiteral("labelIntro"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(labelIntro->sizePolicy().hasHeightForWidth());
        labelIntro->setSizePolicy(sizePolicy);
        labelIntro->setWordWrap(true);

        gridLayout_2->addWidget(labelIntro, 0, 0, 1, 6);

        labelCurrentSpace = new QLabel(selectionPage);
        labelCurrentSpace->setObjectName(QStringLiteral("labelCurrentSpace"));
        labelCurrentSpace->setWordWrap(true);

        gridLayout_2->addWidget(labelCurrentSpace, 1, 0, 1, 6);

        labelDiskSpaceHelp = new QLabel(selectionPage);
        labelDiskSpaceHelp->setObjectName(QStringLiteral("labelDiskSpaceHelp"));
        labelDiskSpaceHelp->setWordWrap(true);

        gridLayout_2->addWidget(labelDiskSpaceHelp, 5, 0, 1, 6);

        line_4 = new QFrame(selectionPage);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_4, 7, 0, 1, 6);

        frame_2 = new QFrame(selectionPage);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Plain);
        verticalLayout_4 = new QVBoxLayout(frame_2);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        labelUsedSpace = new QLabel(frame_2);
        labelUsedSpace->setObjectName(QStringLiteral("labelUsedSpace"));
        sizePolicy.setHeightForWidth(labelUsedSpace->sizePolicy().hasHeightForWidth());
        labelUsedSpace->setSizePolicy(sizePolicy);
        labelUsedSpace->setAutoFillBackground(false);
        labelUsedSpace->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 205);\n"
"border-color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 0);"));
        labelUsedSpace->setFrameShape(QFrame::NoFrame);
        labelUsedSpace->setTextFormat(Qt::PlainText);
        labelUsedSpace->setWordWrap(true);

        verticalLayout_4->addWidget(labelUsedSpace);

        labelFreeSpace = new QLabel(frame_2);
        labelFreeSpace->setObjectName(QStringLiteral("labelFreeSpace"));
        labelFreeSpace->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 205);\n"
"border-color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 0);"));
        labelFreeSpace->setFrameShape(QFrame::NoFrame);
        labelFreeSpace->setTextFormat(Qt::PlainText);
        labelFreeSpace->setWordWrap(true);

        verticalLayout_4->addWidget(labelFreeSpace);


        gridLayout_2->addWidget(frame_2, 2, 0, 1, 6);

        labelIsoName = new QLabel(selectionPage);
        labelIsoName->setObjectName(QStringLiteral("labelIsoName"));

        gridLayout_2->addWidget(labelIsoName, 13, 0, 1, 1);

        buttonSelectSnapshot = new QPushButton(selectionPage);
        buttonSelectSnapshot->setObjectName(QStringLiteral("buttonSelectSnapshot"));
        QIcon icon;
        QString iconThemeName = QStringLiteral("cursor-arrow");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonSelectSnapshot->setIcon(icon);

        gridLayout_2->addWidget(buttonSelectSnapshot, 11, 3, 1, 1);

        labelSnapshotDir = new QLabel(selectionPage);
        labelSnapshotDir->setObjectName(QStringLiteral("labelSnapshotDir"));
        labelSnapshotDir->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 205);\n"
"border-color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 0);"));
        labelSnapshotDir->setFrameShape(QFrame::Box);
        labelSnapshotDir->setWordWrap(true);

        gridLayout_2->addWidget(labelSnapshotDir, 11, 0, 1, 3);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 16, 0, 1, 6);

        lineEditName = new QLineEdit(selectionPage);
        lineEditName->setObjectName(QStringLiteral("lineEditName"));

        gridLayout_2->addWidget(lineEditName, 14, 0, 1, 3);

        stackedWidget->addWidget(selectionPage);
        settingsPage = new QWidget();
        settingsPage->setObjectName(QStringLiteral("settingsPage"));
        gridLayout_4 = new QGridLayout(settingsPage);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_1 = new QLabel(settingsPage);
        label_1->setObjectName(QStringLiteral("label_1"));
        label_1->setWordWrap(true);

        gridLayout_4->addWidget(label_1, 0, 0, 1, 4);

        line = new QFrame(settingsPage);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line, 11, 0, 1, 4);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 14, 0, 1, 1);

        labelExclude = new QLabel(settingsPage);
        labelExclude->setObjectName(QStringLiteral("labelExclude"));
        labelExclude->setWordWrap(true);

        gridLayout_4->addWidget(labelExclude, 4, 0, 1, 4);

        line_5 = new QFrame(settingsPage);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line_5, 7, 0, 1, 4);

        line_2 = new QFrame(settingsPage);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line_2, 3, 0, 1, 4);

        label_2 = new QLabel(settingsPage);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 205);\n"
"border-color: rgb(0, 0, 0);\n"
"color: rgb(0, 0, 0);"));
        label_2->setFrameShape(QFrame::Box);
        label_2->setWordWrap(true);

        gridLayout_4->addWidget(label_2, 1, 0, 1, 4);

        frame = new QFrame(settingsPage);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Sunken);
        frame->setLineWidth(0);
        frame->setMidLineWidth(1);
        gridLayout = new QGridLayout(frame);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        excludeDownloads = new QCheckBox(frame);
        excludeDownloads->setObjectName(QStringLiteral("excludeDownloads"));

        gridLayout->addWidget(excludeDownloads, 1, 1, 1, 1);

        excludeDocuments = new QCheckBox(frame);
        excludeDocuments->setObjectName(QStringLiteral("excludeDocuments"));

        gridLayout->addWidget(excludeDocuments, 1, 0, 1, 1);

        excludePictures = new QCheckBox(frame);
        excludePictures->setObjectName(QStringLiteral("excludePictures"));

        gridLayout->addWidget(excludePictures, 2, 1, 1, 1);

        excludeMusic = new QCheckBox(frame);
        excludeMusic->setObjectName(QStringLiteral("excludeMusic"));

        gridLayout->addWidget(excludeMusic, 2, 0, 1, 1);

        excludeDesktop = new QCheckBox(frame);
        excludeDesktop->setObjectName(QStringLiteral("excludeDesktop"));

        gridLayout->addWidget(excludeDesktop, 1, 2, 1, 1);

        excludeVideos = new QCheckBox(frame);
        excludeVideos->setObjectName(QStringLiteral("excludeVideos"));

        gridLayout->addWidget(excludeVideos, 2, 2, 1, 1);

        excludeNetworks = new QCheckBox(frame);
        excludeNetworks->setObjectName(QStringLiteral("excludeNetworks"));

        gridLayout->addWidget(excludeNetworks, 1, 3, 1, 1);

        excludeAll = new QCheckBox(frame);
        excludeAll->setObjectName(QStringLiteral("excludeAll"));

        gridLayout->addWidget(excludeAll, 3, 0, 1, 1);


        gridLayout_4->addWidget(frame, 5, 0, 1, 4);

        verticalSpacer_3 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_4->addItem(verticalSpacer_3, 2, 0, 1, 1);

        frame_3 = new QFrame(settingsPage);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Plain);
        frame_3->setLineWidth(0);
        gridLayout_3 = new QGridLayout(frame_3);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        radioRespin = new QRadioButton(frame_3);
        radioRespin->setObjectName(QStringLiteral("radioRespin"));

        gridLayout_3->addWidget(radioRespin, 4, 1, 1, 1);

        label = new QLabel(frame_3);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 2, 1, 1, 1);

        radioPersonal = new QRadioButton(frame_3);
        radioPersonal->setObjectName(QStringLiteral("radioPersonal"));
        radioPersonal->setChecked(true);

        gridLayout_3->addWidget(radioPersonal, 3, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 3, 2, 1, 1);


        gridLayout_4->addWidget(frame_3, 13, 0, 1, 4);

        buttonEditExclude = new QPushButton(settingsPage);
        buttonEditExclude->setObjectName(QStringLiteral("buttonEditExclude"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(buttonEditExclude->sizePolicy().hasHeightForWidth());
        buttonEditExclude->setSizePolicy(sizePolicy1);
        QIcon icon1;
        iconThemeName = QStringLiteral("edit");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonEditExclude->setIcon(icon1);

        gridLayout_4->addWidget(buttonEditExclude, 6, 0, 1, 1);

        frame_4 = new QFrame(settingsPage);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Plain);
        frame_4->setLineWidth(0);
        gridLayout_5 = new QGridLayout(frame_4);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        cbCompression = new QComboBox(frame_4);
        cbCompression->addItem(QString());
        cbCompression->addItem(QString());
        cbCompression->addItem(QString());
        cbCompression->addItem(QString());
        cbCompression->setObjectName(QStringLiteral("cbCompression"));

        gridLayout_5->addWidget(cbCompression, 1, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_2, 1, 4, 1, 1);

        label_3 = new QLabel(frame_4);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_5->addWidget(label_3, 0, 0, 1, 1);

        labelCompression = new QLabel(frame_4);
        labelCompression->setObjectName(QStringLiteral("labelCompression"));

        gridLayout_5->addWidget(labelCompression, 1, 0, 1, 2);

        checksums = new QCheckBox(frame_4);
        checksums->setObjectName(QStringLiteral("checksums"));

        gridLayout_5->addWidget(checksums, 2, 0, 1, 2);


        gridLayout_4->addWidget(frame_4, 8, 0, 1, 4);

        stackedWidget->addWidget(settingsPage);
        outputPage = new QWidget();
        outputPage->setObjectName(QStringLiteral("outputPage"));
        verticalLayout_2 = new QVBoxLayout(outputPage);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        outputBox = new QTextEdit(outputPage);
        outputBox->setObjectName(QStringLiteral("outputBox"));
        outputBox->setUndoRedoEnabled(false);
        outputBox->setLineWrapMode(QTextEdit::NoWrap);
        outputBox->setReadOnly(true);
        outputBox->setHtml(QLatin1String("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Noto Sans'; font-size:10.5pt;\"><br /></p></body></html>"));
        outputBox->setOverwriteMode(true);
        outputBox->setAcceptRichText(false);
        outputBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
        outputBox->setPlaceholderText(QStringLiteral(""));

        verticalLayout_2->addWidget(outputBox);

        outputLabel = new QLabel(outputPage);
        outputLabel->setObjectName(QStringLiteral("outputLabel"));
        outputLabel->setWordWrap(true);

        verticalLayout_2->addWidget(outputLabel);

        progressBar = new QProgressBar(outputPage);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        verticalLayout_2->addWidget(progressBar);

        stackedWidget->addWidget(outputPage);

        verticalLayout->addWidget(stackedWidget);

        buttonBar = new QGridLayout();
        buttonBar->setSpacing(5);
        buttonBar->setObjectName(QStringLiteral("buttonBar"));
        buttonBar->setSizeConstraint(QLayout::SetDefaultConstraint);
        buttonBar->setContentsMargins(0, 0, 0, 0);
        buttonHelp = new QPushButton(MainWindow);
        buttonHelp->setObjectName(QStringLiteral("buttonHelp"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(buttonHelp->sizePolicy().hasHeightForWidth());
        buttonHelp->setSizePolicy(sizePolicy2);
        QIcon icon2;
        iconThemeName = QStringLiteral("help-contents");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonHelp->setIcon(icon2);

        buttonBar->addWidget(buttonHelp, 0, 1, 1, 1);

        labelantiX = new QLabel(MainWindow);
        labelantiX->setObjectName(QStringLiteral("labelantiX"));
        labelantiX->setMaximumSize(QSize(32, 32));
        labelantiX->setMidLineWidth(0);
        labelantiX->setPixmap(QPixmap(QString::fromUtf8(":/icons/logo.png")));
        labelantiX->setScaledContents(true);

        buttonBar->addWidget(labelantiX, 0, 3, 1, 1);

        horizontalSpacer2 = new QSpacerItem(100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        buttonBar->addItem(horizontalSpacer2, 0, 5, 1, 1);

        buttonAbout = new QPushButton(MainWindow);
        buttonAbout->setObjectName(QStringLiteral("buttonAbout"));
        sizePolicy2.setHeightForWidth(buttonAbout->sizePolicy().hasHeightForWidth());
        buttonAbout->setSizePolicy(sizePolicy2);
        QIcon icon3;
        iconThemeName = QStringLiteral("help-about");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonAbout->setIcon(icon3);
        buttonAbout->setAutoDefault(true);

        buttonBar->addWidget(buttonAbout, 0, 0, 1, 1);

        buttonCancel = new QPushButton(MainWindow);
        buttonCancel->setObjectName(QStringLiteral("buttonCancel"));
        sizePolicy2.setHeightForWidth(buttonCancel->sizePolicy().hasHeightForWidth());
        buttonCancel->setSizePolicy(sizePolicy2);
        QIcon icon4;
        iconThemeName = QStringLiteral("window-close");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonCancel->setIcon(icon4);
        buttonCancel->setAutoDefault(true);

        buttonBar->addWidget(buttonCancel, 0, 10, 1, 1);

        buttonNext = new QPushButton(MainWindow);
        buttonNext->setObjectName(QStringLiteral("buttonNext"));
        sizePolicy2.setHeightForWidth(buttonNext->sizePolicy().hasHeightForWidth());
        buttonNext->setSizePolicy(sizePolicy2);
        QIcon icon5;
        iconThemeName = QStringLiteral("go-next");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonNext->setIcon(icon5);
        buttonNext->setAutoDefault(true);

        buttonBar->addWidget(buttonNext, 0, 8, 1, 1);

        horizontalSpacer1 = new QSpacerItem(100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        buttonBar->addItem(horizontalSpacer1, 0, 2, 1, 1);

        buttonBack = new QPushButton(MainWindow);
        buttonBack->setObjectName(QStringLiteral("buttonBack"));
        sizePolicy2.setHeightForWidth(buttonBack->sizePolicy().hasHeightForWidth());
        buttonBack->setSizePolicy(sizePolicy2);
        QIcon icon6;
        iconThemeName = QStringLiteral("go-previous");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        buttonBack->setIcon(icon6);

        buttonBar->addWidget(buttonBack, 0, 7, 1, 1);


        verticalLayout->addLayout(buttonBar);


        retranslateUi(MainWindow);
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeDesktop, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeDocuments, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeDownloads, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeMusic, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludePictures, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeVideos, SLOT(setChecked(bool)));
        QObject::connect(excludeAll, SIGNAL(clicked(bool)), excludeNetworks, SLOT(setChecked(bool)));

        stackedWidget->setCurrentIndex(0);
        buttonNext->setDefault(true);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Snapshot", nullptr));
        labelLocation->setText(QApplication::translate("MainWindow", "Snapshot location:", nullptr));
        labelIntro->setText(QApplication::translate("MainWindow", "<html><head/><body><p>Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.<br/></p></body></html>", nullptr));
        labelCurrentSpace->setText(QApplication::translate("MainWindow", "Used space on / (root) and /home partitions:", nullptr));
        labelDiskSpaceHelp->setText(QString());
        labelUsedSpace->setText(QString());
        labelFreeSpace->setText(QString());
        labelIsoName->setText(QApplication::translate("MainWindow", "Snapshot name:", nullptr));
        buttonSelectSnapshot->setText(QApplication::translate("MainWindow", "Select a different snapshot directory", nullptr));
        labelSnapshotDir->setText(QString());
        label_1->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        labelExclude->setText(QApplication::translate("MainWindow", "You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        excludeDownloads->setText(QApplication::translate("MainWindow", "Downloads", nullptr));
        excludeDocuments->setText(QApplication::translate("MainWindow", "Documents", nullptr));
        excludePictures->setText(QApplication::translate("MainWindow", "Pictures", nullptr));
        excludeMusic->setText(QApplication::translate("MainWindow", "Music", nullptr));
        excludeDesktop->setText(QApplication::translate("MainWindow", "Desktop", nullptr));
        excludeVideos->setText(QApplication::translate("MainWindow", "Videos", nullptr));
#ifndef QT_NO_STATUSTIP
        excludeNetworks->setStatusTip(QApplication::translate("MainWindow", "exclude network configurations", nullptr));
#endif // QT_NO_STATUSTIP
        excludeNetworks->setText(QApplication::translate("MainWindow", "Networks", nullptr));
        excludeAll->setText(QApplication::translate("MainWindow", "All of the above", nullptr));
#ifndef QT_NO_TOOLTIP
        radioRespin->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>This option will reset &quot;demo&quot; and &quot;root&quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        radioRespin->setText(QApplication::translate("MainWindow", "Resetting accounts (for distribution to others)", nullptr));
        label->setText(QApplication::translate("MainWindow", "Type of snapshot:", nullptr));
        radioPersonal->setText(QApplication::translate("MainWindow", "Preserving accounts (for personal backup)", nullptr));
        buttonEditExclude->setText(QApplication::translate("MainWindow", "Edit Exclusion File", nullptr));
        cbCompression->setItemText(0, QApplication::translate("MainWindow", "lz4", nullptr));
        cbCompression->setItemText(1, QApplication::translate("MainWindow", "lzo", nullptr));
        cbCompression->setItemText(2, QApplication::translate("MainWindow", "gzip", nullptr));
        cbCompression->setItemText(3, QApplication::translate("MainWindow", "xz", nullptr));

        label_3->setText(QApplication::translate("MainWindow", "Options:", nullptr));
        labelCompression->setText(QApplication::translate("MainWindow", "ISO compression scheme:", nullptr));
        checksums->setText(QApplication::translate("MainWindow", "Calculate checksums", nullptr));
        outputLabel->setText(QString());
#ifndef QT_NO_TOOLTIP
        buttonHelp->setToolTip(QApplication::translate("MainWindow", "Display help ", nullptr));
#endif // QT_NO_TOOLTIP
        buttonHelp->setText(QApplication::translate("MainWindow", "Help", nullptr));
#ifndef QT_NO_SHORTCUT
        buttonHelp->setShortcut(QApplication::translate("MainWindow", "Alt+H", nullptr));
#endif // QT_NO_SHORTCUT
        labelantiX->setText(QString());
#ifndef QT_NO_TOOLTIP
        buttonAbout->setToolTip(QApplication::translate("MainWindow", "About this application", nullptr));
#endif // QT_NO_TOOLTIP
        buttonAbout->setText(QApplication::translate("MainWindow", "About...", nullptr));
#ifndef QT_NO_SHORTCUT
        buttonAbout->setShortcut(QApplication::translate("MainWindow", "Alt+B", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        buttonCancel->setToolTip(QApplication::translate("MainWindow", "Quit application", nullptr));
#endif // QT_NO_TOOLTIP
        buttonCancel->setText(QApplication::translate("MainWindow", "Cancel", nullptr));
#ifndef QT_NO_SHORTCUT
        buttonCancel->setShortcut(QApplication::translate("MainWindow", "Alt+N", nullptr));
#endif // QT_NO_SHORTCUT
        buttonNext->setText(QApplication::translate("MainWindow", "Next", nullptr));
#ifndef QT_NO_SHORTCUT
        buttonNext->setShortcut(QString());
#endif // QT_NO_SHORTCUT
        buttonBack->setText(QApplication::translate("MainWindow", "Back", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
